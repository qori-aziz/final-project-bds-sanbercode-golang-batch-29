package models

type (
	// Role
	Role struct {
		ID          uint   `json:"id" gorm:"primary_key"`
		Name        string `gorm:"not null;unique" json:"name"`
		Description string `json:"description"`
		User        []User `json:"-"`
	}
)
