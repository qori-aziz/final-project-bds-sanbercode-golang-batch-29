package models

type (
	// Order
	PersonalData struct {
		ID      uint   `gorm:"primary_key" json:"id"`
		Address string `json:"address"`
		BankAcc string `json:"bankacc"`
	}
)
