package controllers

import (
	"net/http"
	"time"

	"proyek-ecom-qori/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type goodsInput struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Price           uint   `json:"price"`
	GoodsCategoryID uint   `json:"goods_category_id"`
	SellerID        uint   `json:"seller_id"`
}

// GetAllGoods godoc
// @Summary Get all Goods.
// @Description Get a list of Goods.
// @Tags Goods
// @Produce json
// @Success 200 {object} []models.Goods
// @Router /goods [get]
func GetAllGoods(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var goods []models.Goods
	db.Find(&goods)

	c.JSON(http.StatusOK, gin.H{"data": goods})
}

// CreateGoods godoc
// @Summary Create New Goods.
// @Description Creating a new Goods. Only seller can create new goods.
// @Tags Goods
// @Param Body body goodsInput true "the body to create a new Goods"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Goods
// @Router /goods [post]
func CreateGoods(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input goodsInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//Validate category
	var category models.GoodsCategory
	if err := db.Where("id = ?", input.GoodsCategoryID).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "GoodsCategoryID not found!"})
		return
	}

	//Validate User
	var seller models.User
	if err := db.Where("id = ?", input.SellerID).First(&seller).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
		return
	}

	//Validate Seller, role_id 2 adalah penjual
	if err := db.Where("id = ?", input.SellerID).Where("role_id = ?", 2).First(&seller).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "SellerID not found! User may not be a seller"})
		return
	}

	// Create Rating
	goods := models.Goods{Name: input.Name, Description: input.Description, Price: input.Price, GoodsCategoryID: input.GoodsCategoryID, SellerID: input.SellerID}

	db.Create(&goods)

	c.JSON(http.StatusOK, gin.H{"data": goods})
}

// GetGoodsById godoc
// @Summary Get Goods.
// @Description Get an Goods by id.
// @Tags Goods
// @Produce json
// @Param id path string true "Goods id"
// @Success 200 {object} models.Goods
// @Router /goods/{id} [get]
func GetGoodsById(c *gin.Context) { // Get model if exist
	var goods models.Goods

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&goods).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": goods})
}

// UpdateGoods godoc
// @Summary Update Goods.
// @Description Update Goods by id.
// @Tags Goods
// @Produce json
// @Param id path string true "Goods id"
// @Param Body body goodsInput true "the body to update goods"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Goods
// @Router /goods/{id} [patch]
func UpdateGoods(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var goods models.Goods
	if err := db.Where("id = ?", c.Param("id")).First(&goods).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input goodsInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//Validate category
	var category models.GoodsCategory
	if err := db.Where("id = ?", input.GoodsCategoryID).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "GoodsCategoryID not found!"})
		return
	}

	//Validate seller
	var seller models.User
	if err := db.Where("id = ?", input.SellerID).First(&seller).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "SellerID not found!"})
		return
	}

	//Validate Seller, role_id 2 adalah penjual
	if err := db.Where("id = ?", input.SellerID).Where("role_id = ?", 2).First(&seller).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "SellerID not found! User may not be a seller"})
		return
	}

	var updatedInput models.Goods
	updatedInput.Name = input.Name
	updatedInput.Description = input.Description
	updatedInput.Price = input.Price
	updatedInput.GoodsCategoryID = input.GoodsCategoryID
	updatedInput.SellerID = input.SellerID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&goods).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": goods})
}

// DeleteGoodsgodoc
// @Summary Delete one Goods.
// @Description Delete a Goods by id.
// @Tags Goods
// @Produce json
// @Param id path string true "Goods id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /goods/{id} [delete]
func DeleteGoods(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var goods models.Goods
	if err := db.Where("id = ?", c.Param("id")).First(&goods).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&goods)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
