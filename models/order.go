package models

import (
	"time"
)

type (
	// Order
	Order struct {
		ID             uint         `gorm:"primary_key" json:"id"`
		Quantity       uint         `gorm:"not null;" json:"quantity"`
		TotalPrice     uint         `gorm:"not null;" json:"total_price"`
		Rating         float64      `gorm:"not null;" json:"rating"`
		ProveID        uint         `gorm:"not null;unique" json:"prove_id"`
		BuyerID        uint         `gorm:"not null;" json:"buyer_id"`
		GoodsID        uint         `gorm:"not null;" json:"goods_id"`
		PersonalDataID uint         `gorm:"not null;" json:"personal_data_id"`
		CreatedAt      time.Time    `json:"created_at"`
		UpdatedAt      time.Time    `json:"updated_at"`
		Prove          Prove        `json:"-"`
		Buyer          User         `json:"-"`
		Goods          Goods        `json:"-"`
		PersonalData   PersonalData `json:"-"`
	}
)
