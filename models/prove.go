package models

import "net/url"

type (
	// Order
	Prove struct {
		ID           uint   `gorm:"primary_key" json:"id"`
		Photo_URL    string `json:"photo_url"`
		Photo_Format string `json:"photo_format"`
		Virtual_Acc  string `json:"virtual_acc"`
	}
)

//Validasi URL untuk memvalidasi input Photo_URL
func IsValidUrl(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}

	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}
