package controllers

import (
	"net/http"

	"proyek-ecom-qori/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type orderInput struct {
	Quantity       uint    `json:"quantity"`
	TotalPrice     uint    `json:"total_price"`
	Rating         float64 `json:"rating"`
	ProveID        uint    `json:"prove_id"`
	BuyerID        uint    `json:"buyer_id"`
	GoodsID        uint    `json:"goods_id"`
	PersonalDataID uint    `json:"personal_data_id"`
}

// GetAllOrders godoc
// @Summary Get all Orders.
// @Description Get a list of Orders.
// @Tags Order
// @Produce json
// @Success 200 {object} []models.Order
// @Router /order [get]
func GetAllOrders(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var orders []models.Order
	db.Find(&orders)

	c.JSON(http.StatusOK, gin.H{"data": orders})
}

// CreateOrder godoc
// @Summary Create New Order.
// @Description Creating a new Order. Only buyer can create order.
// @Tags Order
// @Param Body body orderInput true "the body to create a new order"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Order
// @Router /order [post]
func CreateOrder(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var input orderInput

	var prove models.Prove
	var buyer models.User
	var goods models.Goods
	var personalData models.PersonalData

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", input.ProveID).First(&prove).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ProveID not found!"})
		return
	}

	if err := db.Where("id = ?", input.BuyerID).First(&buyer).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User not found!"})
		return
	}

	//Role ID 1 adalah pembeli, 2 adalah penjual
	if isBuyer := db.Where("id = ?", input.BuyerID).Where("role_id = ?", 1).First(&buyer).Error; isBuyer != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "BuyerID not found! User is not a buyer"})
		return
	}

	if err := db.Where("id = ?", input.GoodsID).First(&goods).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "GoodsID not found!"})
		return
	}

	if err := db.Where("id = ?", input.PersonalDataID).First(&personalData).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "PersonalDataID not found!"})
		return
	}

	// Create Order
	order := models.Order{Quantity: input.Quantity, TotalPrice: input.TotalPrice, Rating: input.Rating, ProveID: input.ProveID, BuyerID: input.BuyerID, GoodsID: input.GoodsID, PersonalDataID: input.PersonalDataID}
	db.Create(&order)

	c.JSON(http.StatusOK, gin.H{"data": order})
}

// GetOrderById godoc
// @Summary Get Order.
// @Description Get a Order by id.
// @Tags Order
// @Produce json
// @Param id path string true "order id"
// @Success 200 {object} models.Order
// @Router /order/{id} [get]
func GetOrderById(c *gin.Context) { // Get model if exist
	var order models.Order

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": order})
}

// UpdateOrder godoc
// @Summary Update Order.
// @Description Update order by id. Only buyer can update order.
// @Tags Order
// @Produce json
// @Param id path string true "order id"
// @Param Body body orderInput true "the body to update an order"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Order
// @Router /order/{id} [patch]
func UpdateOrder(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var order models.Order
	var prove models.Prove
	var buyer models.User
	var goods models.Goods
	var personalData models.PersonalData

	if err := db.Where("id = ?", c.Param("id")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input orderInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", input.ProveID).First(&prove).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ProveID not found!"})
		return
	}

	if err := db.Where("id = ?", input.BuyerID).First(&buyer).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User not found!"})
		return
	}

	//Role ID 1 adalah pembeli, 2 adalah penjual
	if isBuyer := db.Where("id = ?", input.BuyerID).Where("role_id = ?", 1).First(&buyer).Error; isBuyer != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "BuyerID not found! User is not a buyer"})
		return
	}

	if err := db.Where("id = ?", input.GoodsID).First(&goods).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "GoodsID not found!"})
		return
	}

	if err := db.Where("id = ?", input.PersonalDataID).First(&personalData).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "PersonalDataID not found!"})
		return
	}

	var updatedInput models.Order
	updatedInput.Quantity = input.Quantity
	updatedInput.TotalPrice = input.TotalPrice
	updatedInput.Rating = input.Rating
	updatedInput.ProveID = input.ProveID
	updatedInput.BuyerID = input.BuyerID
	updatedInput.GoodsID = input.GoodsID
	updatedInput.PersonalDataID = input.PersonalDataID

	db.Model(&order).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": order})
}

// DeleteOrder godoc
// @Summary Delete one order.
// @Description Delete an order by id.
// @Tags Order
// @Produce json
// @Param id path string true "order id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /order/{id} [delete]
func DeleteOrder(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var order models.Order
	if err := db.Where("id = ?", c.Param("id")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&order)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
