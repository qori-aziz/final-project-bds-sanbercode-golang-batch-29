INSERT INTO public.goods ("name",description,price,goods_category_id,seller_id,created_at,updated_at) VALUES
	 ('sabun','ya sabun',1000,1,2,'2021-11-22 09:01:52.113449+07','2021-11-22 09:01:52.113449+07');INSERT INTO public.goods_categories ("name",description,created_at,updated_at) VALUES
	 ('alat mandi','berisi peralatan mandi','2021-11-22 09:01:20.217079+07','2021-11-22 09:01:20.217079+07'),
	 ('mobil','berisi mobil','2021-11-22 09:01:39.428864+07','2021-11-22 09:01:39.428864+07');INSERT INTO public.orders (quantity,total_price,rating,prove_id,buyer_id,goods_id,personal_data_id,created_at,updated_at) VALUES
	 (100,150000,4.5,1,1,1,1,'2021-11-22 09:12:52.567777+07','2021-11-22 09:12:52.567777+07');INSERT INTO public.personal_data (address,bank_acc) VALUES
	 ('Indonesia bagian selatan','123AB456');INSERT INTO public.proves (photo_url,photo_format,virtual_acc) VALUES
	 ('https://www.google.com/','jpeg','0123456ABC');INSERT INTO public.roles ("name",description) VALUES
	 ('pembeli','yang membeli barang'),
	 ('penjual','yang menjual barang');INSERT INTO public.users (username,email,"password",role_id,created_at,updated_at) VALUES
	 ('qori','muhamadqori@gmail.com','$2a$10$jdkn7Gl6M3/9K4yopyiIQeFHnYnT5w5W3bJIlEWCRiZl76z2KT.1i',1,'2021-11-22 08:50:08.090293+07','2021-11-22 08:50:08.090293+07'),
	 ('qori1','muhamadqori1@gmail.com','$2a$10$i.QiEHM6qVrNjaPZA4KDl.EWVXALuRt4ZpOydiXjrYF1g5tqGSnZy',2,'2021-11-22 08:50:24.889705+07','2021-11-22 08:50:24.889705+07');