package controllers

import (
	"net/http"
	"time"

	"proyek-ecom-qori/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type goodsCategoryInput struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

// GetAllGoodsCategory godoc
// @Summary Get all GoodsCategory.
// @Description Get a list of GoodsCategory.
// @Tags GoodsCategory
// @Produce json
// @Success 200 {object} []models.GoodsCategory
// @Router /goods-category [get]
func GetAllGoodsCategory(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var goodsCategory []models.GoodsCategory
	db.Find(&goodsCategory)

	c.JSON(http.StatusOK, gin.H{"data": goodsCategory})
}

// CreateGoodsCategory godoc
// @Summary Create New GoodsCategory.
// @Description Creating a new Goods Category.
// @Tags GoodsCategory
// @Param Body body goodsCategoryInput true "the body to create a new Goods Category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.GoodsCategory
// @Router /goods-category [post]
func CreateGoodsCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input goodsCategoryInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Create Goods Category
	goodsCategory := models.GoodsCategory{Name: input.Name, Description: input.Description}

	db.Create(&goodsCategory)

	c.JSON(http.StatusOK, gin.H{"data": goodsCategory})
}

// GetGoodsCategoryById godoc
// @Summary Get GoodsCategory.
// @Description Get a GoodsCategory by id.
// @Tags GoodsCategory
// @Produce json
// @Param id path string true "GoodsCategory id"
// @Success 200 {object} models.GoodsCategory
// @Router /goods-category/{id} [get]
func GetGoodsCategoryById(c *gin.Context) { // Get model if exist
	var goodsCategory models.GoodsCategory

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&goodsCategory).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": goodsCategory})
}

// GetGoodsByGoodsCategoryId godoc
// @Summary Get Goods.
// @Description Get all Goods by GoodsCategoryId.
// @Tags GoodsCategory
// @Produce json
// @Param id path string true "GoodsCategory id"
// @Success 200 {object} []models.Goods
// @Router /goods-category/{id}/goods [get]
func GetGoodsByCategoryId(c *gin.Context) { // Get model if exist
	var goods []models.Goods

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("goods_category_id = ?", c.Param("id")).Find(&goods).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": goods})
}

// UpdateGoodsCategory godoc
// @Summary Update GoodsCategory.
// @Description Update GoodsCategory by id.
// @Tags GoodsCategory
// @Produce json
// @Param id path string true "GoodsCategory id"
// @Param Body body goodsInput true "the body to update goods categiry"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.GoodsCategory
// @Router /goods-category/{id} [patch]
func UpdateGoodsCategory(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var goodsCategory models.GoodsCategory
	if err := db.Where("id = ?", c.Param("id")).First(&goodsCategory).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input goodsInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.GoodsCategory
	updatedInput.Name = input.Name
	updatedInput.Description = input.Description
	updatedInput.UpdatedAt = time.Now()

	db.Model(&goodsCategory).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": goodsCategory})
}

// DeleteGoodsCategory godoc
// @Summary Delete one Goods Category.
// @Description Delete a Goods by id.
// @Tags Goods
// @Produce json
// @Param id path string true "GoodsCategory id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /goods-category/{id} [delete]
func DeleteGoodsCategory(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var goodsCategory models.GoodsCategory
	if err := db.Where("id = ?", c.Param("id")).First(&goodsCategory).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&goodsCategory)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
