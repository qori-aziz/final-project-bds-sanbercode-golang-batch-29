package routes

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"proyek-ecom-qori/controllers"
	"proyek-ecom-qori/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})
	//////authController
	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)

	//////goodsCategoryController
	r.GET("/goods-category", controllers.GetAllGoodsCategory)
	r.GET("/goods-category/:id", controllers.GetGoodsCategoryById)
	r.GET("/goods-category/:id/goods", controllers.GetGoodsByCategoryId)

	goodsCategoryMiddlewareRoute := r.Group("/goods-category")
	goodsCategoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	goodsCategoryMiddlewareRoute.POST("/", controllers.CreateGoodsCategory)
	goodsCategoryMiddlewareRoute.PATCH("/:id", controllers.UpdateGoodsCategory)
	goodsCategoryMiddlewareRoute.DELETE("/:id", controllers.DeleteGoodsCategory)

	///////goodsController
	r.GET("/goods", controllers.GetAllGoods)
	r.GET("/goods/:id", controllers.GetGoodsById)

	goodsMiddlewareRoute := r.Group("/goods")
	goodsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	goodsMiddlewareRoute.POST("/", controllers.CreateGoods)
	goodsMiddlewareRoute.PATCH("/:id", controllers.UpdateGoods)
	goodsMiddlewareRoute.DELETE("/:id", controllers.DeleteGoods)

	//////orderController
	r.GET("/order", controllers.GetAllOrders)
	r.GET("/order/:id", controllers.GetOrderById)

	orderCategoryMiddlewareRoute := r.Group("/order")
	orderCategoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	orderCategoryMiddlewareRoute.POST("/", controllers.CreateOrder)
	orderCategoryMiddlewareRoute.PATCH("/:id", controllers.UpdateOrder)
	orderCategoryMiddlewareRoute.DELETE("/:id", controllers.DeleteOrder)

	//////personalDataController
	r.GET("/personal-data", controllers.GetAllPersonalData)
	r.GET("/personal-data/:id", controllers.GetPersonalDataById)

	personalDataCategoryMiddlewareRoute := r.Group("/personal-data")
	personalDataCategoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	personalDataCategoryMiddlewareRoute.POST("/", controllers.CreatePersonalData)
	personalDataCategoryMiddlewareRoute.PATCH("/:id", controllers.UpdatePersonalData)
	personalDataCategoryMiddlewareRoute.DELETE("/:id", controllers.DeletePersonalData)

	//////proveController
	r.GET("/prove", controllers.GetAllProve)
	r.GET("/prove/:id", controllers.GetProveById)
	r.GET("/prove/:id/order", controllers.GetOrderByProveId)

	proveDataCategoryMiddlewareRoute := r.Group("/prove")
	proveDataCategoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	proveDataCategoryMiddlewareRoute.POST("/", controllers.CreateProves)
	proveDataCategoryMiddlewareRoute.PATCH("/:id", controllers.UpdateProve)
	proveDataCategoryMiddlewareRoute.DELETE("/:id", controllers.DeleteProve)

	//////roleController
	r.GET("/role", controllers.GetAllRoles)
	r.GET("/role/:id", controllers.GetRoleById)
	r.GET("/role/:id/user", controllers.GetUserByRoleId)
	r.POST("/role", controllers.CreateRoles)
	r.PATCH("/role/:id", controllers.UpdateRole)
	r.DELETE("/role/:id", controllers.DeleteRole)

	/*
		roleDataCategoryMiddlewareRoute := r.Group("/role")
		roleDataCategoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
		roleDataCategoryMiddlewareRoute.POST("/", controllers.CreateRoles)
		roleDataCategoryMiddlewareRoute.PATCH("/:id", controllers.UpdateRole)
		roleDataCategoryMiddlewareRoute.DELETE("/:id", controllers.DeleteRole)
	*/

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
