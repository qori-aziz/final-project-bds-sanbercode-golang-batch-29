package controllers

import (
	"net/http"

	"proyek-ecom-qori/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type proveInput struct {
	Photo_URL    string `json:"photo_url"`
	Photo_Format string `json:"photo_format"`
	Virtual_Acc  string `json:"virtual_acc"`
}

// GetAllProve godoc
// @Summary Get all Prove.
// @Description Get a list of Prove.
// @Tags Prove
// @Produce json
// @Success 200 {object} []models.Prove
// @Router /prove [get]
func GetAllProve(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var proves []models.Prove
	db.Find(&proves)

	c.JSON(http.StatusOK, gin.H{"data": proves})
}

// CreateProves godoc
// @Summary Create New Prove.
// @Description Creating a new Prove.
// @Tags Prove
// @Param Body body proveInput true "the body to create a new Prove"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Prove
// @Router /prove [post]
func CreateProves(c *gin.Context) {
	// Validate input
	var input proveInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//Validasi input URL
	if isURL := models.IsValidUrl(input.Photo_URL); !isURL {
		c.JSON(http.StatusBadRequest, gin.H{"error": "URL not VALID"})
		return
	}

	// Create Role
	prove := models.Prove{Photo_URL: input.Photo_URL, Photo_Format: input.Photo_Format, Virtual_Acc: input.Virtual_Acc}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&prove)

	c.JSON(http.StatusOK, gin.H{"data": prove})
}

// GetProveById godoc
// @Summary Get Prove.
// @Description Get an Prove by id.
// @Tags Prove
// @Produce json
// @Param id path string true "Prove id"
// @Success 200 {object} models.Prove
// @Router /prove/{id} [get]
func GetProveById(c *gin.Context) { // Get model if exist
	var prove models.Prove

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&prove).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": prove})
}

// GetOrderByProveId godoc
// @Summary Get Prove.
// @Description Get Order by Prove Id.
// @Tags Prove
// @Produce json
// @Param id path string true "Prove id"
// @Success 1 {object} models.Order
// @Router /prove/{id}/order [get]
func GetOrderByProveId(c *gin.Context) { // Get model if exist
	var order models.Order

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("prove_id = ?", c.Param("id")).Find(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": order})
}

// UpdateProve godoc
// @Summary Update Prove.
// @Description Update Prove by id.
// @Tags Prove
// @Produce json
// @Param id path string true "Prove id"
// @Param Body body proveInput true "the body to update prove"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Prove
// @Router /prove/{id} [patch]
func UpdateProve(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var prove models.Prove
	if err := db.Where("id = ?", c.Param("id")).First(&prove).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input proveInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//Validasi input URL
	if isURL := models.IsValidUrl(input.Photo_URL); !isURL {
		c.JSON(http.StatusBadRequest, gin.H{"error": "URL not VALID"})
		return
	}

	var updatedInput models.Prove
	updatedInput.Photo_URL = input.Photo_URL
	updatedInput.Photo_Format = input.Photo_Format
	updatedInput.Virtual_Acc = input.Virtual_Acc

	db.Model(&prove).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": prove})
}

// DeleteProve godoc
// @Summary Delete one Prove.
// @Description Delete a Prove by id.
// @Tags Prove
// @Produce json
// @Param id path string true "Prove id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /prove/{id} [delete]
func DeleteProve(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var prove models.Prove
	if err := db.Where("id = ?", c.Param("id")).First(&prove).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&prove)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
