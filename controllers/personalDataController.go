package controllers

import (
	"net/http"

	"proyek-ecom-qori/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type personalDataInput struct {
	Address string `json:"address"`
	BankAcc string `json:"bankacc"`
}

// GetAllPersonalData godoc
// @Summary Get all PersonalData.
// @Description Get a list of PersonalData.
// @Tags PersonalData
// @Produce json
// @Success 200 {object} []models.PersonalData
// @Router /personal-data [get]
func GetAllPersonalData(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var personalData []models.PersonalData
	db.Find(&personalData)

	c.JSON(http.StatusOK, gin.H{"data": personalData})
}

// CreatePersonalData godoc
// @Summary Create New PersonalData.
// @Description Creating a new Personal Data.
// @Tags PersonalData
// @Param Body body personalDataInput true "the body to create a new Personal Data"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.PersonalData
// @Router /personal-data [post]
func CreatePersonalData(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input personalDataInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Create Personal Data
	personalData := models.PersonalData{Address: input.Address, BankAcc: input.BankAcc}

	db.Create(&personalData)

	c.JSON(http.StatusOK, gin.H{"data": personalData})
}

// GetPersonalDataById godoc
// @Summary Get PersonalData.
// @Description Get a PersonalData by id.
// @Tags PersonalData
// @Produce json
// @Param id path string true "PersonalData id"
// @Success 200 {object} models.PersonalData
// @Router /personal-data/{id} [get]
func GetPersonalDataById(c *gin.Context) { // Get model if exist
	var personalData models.PersonalData

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&personalData).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": personalData})
}

// UpdatePersonalData godoc
// @Summary Update PersonalData.
// @Description Update PersonalData by id.
// @Tags PersonalData
// @Produce json
// @Param id path string true "PersonalData id"
// @Param Body body personalDataInput true "the body to update personal data"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.PersonalData
// @Router /personal-data/{id} [patch]
func UpdatePersonalData(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var personalData models.PersonalData
	if err := db.Where("id = ?", c.Param("id")).First(&personalData).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input personalDataInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.PersonalData
	updatedInput.Address = input.Address
	updatedInput.BankAcc = input.BankAcc

	db.Model(&personalData).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": personalData})
}

// DeletePersonalData godoc
// @Summary Delete one Personal Data.
// @Description Delete a Personal Data by id.
// @Tags PersonalData
// @Produce json
// @Param id path string true "PersonalData id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /personal-data/{id} [delete]
func DeletePersonalData(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var personalData models.PersonalData
	if err := db.Where("id = ?", c.Param("id")).First(&personalData).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&personalData)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
