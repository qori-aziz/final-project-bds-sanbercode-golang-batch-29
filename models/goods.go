package models

import (
	"time"
)

type (
	// AgeRatingCategory
	Goods struct {
		ID              uint          `gorm:"primary_key" json:"id"`
		Name            string        `json:"name"`
		Description     string        `json:"description"`
		Price           uint          `json:"price"`
		GoodsCategoryID uint          `json:"goods_category_id"`
		SellerID        uint          `json:"seller_id"`
		CreatedAt       time.Time     `json:"created_at"`
		UpdatedAt       time.Time     `json:"updated_at"`
		GoodsCategory   GoodsCategory `json:"-"`
		Seller          User          `json:"-"`
	}
)
