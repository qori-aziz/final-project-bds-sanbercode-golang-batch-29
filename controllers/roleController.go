package controllers

import (
	"net/http"

	"proyek-ecom-qori/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type roleInput struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

// GetAllRoles godoc
// @Summary Get all Role.
// @Description Get a list of Role.
// @Tags Role
// @Produce json
// @Success 200 {object} []models.Role
// @Router /role [get]
func GetAllRoles(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var roles []models.Role
	db.Find(&roles)

	c.JSON(http.StatusOK, gin.H{"data": roles})
}

// CreateRoles godoc
// @Summary Create New Role.
// @Description Creating a new Role.
// @Tags Role
// @Param Body body roleInput true "the body to create a new Role"
// @Produce json
// @Success 200 {object} models.Role
// @Router /role [post]
func CreateRoles(c *gin.Context) {
	// Validate input
	var input roleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Role
	role := models.Role{Name: input.Name, Description: input.Description}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&role)

	c.JSON(http.StatusOK, gin.H{"data": role})
}

// GetRoleById godoc
// @Summary Get Role.
// @Description Get an Role by id.
// @Tags Role
// @Produce json
// @Param id path string true "Role id"
// @Success 200 {object} models.Role
// @Router /role/{id} [get]
func GetRoleById(c *gin.Context) { // Get model if exist
	var role models.Role

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&role).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": role})
}

// GetUserByRoleId godoc
// @Summary Get User.
// @Description Get all User by Role Id.
// @Tags Role
// @Produce json
// @Param id path string true "Role id"
// @Success 200 {object} []models.User
// @Router /role/{id}/user [get]
func GetUserByRoleId(c *gin.Context) { // Get model if exist
	var users []models.User

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("role_id = ?", c.Param("id")).Find(&users).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": users})
}

// UpdateRole godoc
// @Summary Update Role.
// @Description Update Role by id.
// @Tags Role
// @Produce json
// @Param id path string true "Role id"
// @Param Body body roleInput true "the body to update role"
// @Success 200 {object} models.Role
// @Router /role/{id} [patch]
func UpdateRole(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var role models.Role
	if err := db.Where("id = ?", c.Param("id")).First(&role).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input roleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Role
	updatedInput.Name = input.Name
	updatedInput.Description = input.Description

	db.Model(&role).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": role})
}

// DeleteRole godoc
// @Summary Delete one Role.
// @Description Delete a Role by id.
// @Tags Role
// @Produce json
// @Param id path string true "Role id"
// @Success 200 {object} map[string]boolean
// @Router /role/{id} [delete]
func DeleteRole(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var role models.Role
	if err := db.Where("id = ?", c.Param("id")).First(&role).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&role)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
